package com.example.user.mvp.Model;

/**
 * Created by user on 02.11.17.
 */

public class City {

    private String name;
    private int population;

    public City(String name, int population) {
        this.name = name;
        this.population = population;
    }

    public String getName() {
        return name;
    }

    public int getPopulation() {
        return population;
    }

}
