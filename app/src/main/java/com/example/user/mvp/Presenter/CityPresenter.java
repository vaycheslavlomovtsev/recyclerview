package com.example.user.mvp.Presenter;


import android.support.v7.widget.RecyclerView;
import android.util.Log;
import com.example.user.mvp.MainActivity;
import com.example.user.mvp.Model.CityRepository;
import java.io.Serializable;
import static android.content.ContentValues.TAG;

/**
 * Created by user on 07.11.17.
 */

public class CityPresenter implements Serializable {

    private CityAdapter cityAdapter;
    private MainActivity view;

    private int sortType = CityRepository.RAND_ORDER;


    public RecyclerView.Adapter getAdapter(RecyclerView recyclerView) {
        if (cityAdapter == null) {
            cityAdapter = new CityAdapter(recyclerView);
        }
        return cityAdapter;
    }

    public void onResume() {
        updateList();
    }

    public void onOptionsMenuItemAzSortClick() {
        sortType = CityRepository.AZ_ORDER;
        updateList();
    }

    public void onOptionsMenuItemPopSortClick() {
        sortType = CityRepository.HZ_ORDER;
        updateList();
    }

    public void onOptionsMenuItemRandSortClick() {
        sortType = CityRepository.RAND_ORDER;
        updateList();
    }

    public void updateList() {
        cityAdapter.setDataset(CityRepository.getInstance().getCities(sortType));
        cityAdapter.itemTouchHelper.attachToRecyclerView(MainActivity.recyclerView);
    }

    public void attachView(MainActivity view) {
        this.view = view;
        updateList();
        Log.i(TAG, "attachView()");
    }

    public void detachView() {
        view = null;
        Log.i(TAG, "DetachView()");
    }
}
