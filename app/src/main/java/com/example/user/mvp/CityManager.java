package com.example.user.mvp;

import com.example.user.mvp.Presenter.CityPresenter;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by user on 14.11.17.
 */

public class CityManager {
    private static CityManager Instance;

    static CityManager getInstance() {
        if (Instance == null) {
            Instance = new CityManager();
        }
        return Instance;
    }

    private HashMap<String, Serializable> presenterHash = new HashMap<>();

    public HashMap<String, Serializable> getPresenterHash() {
        return presenterHash;
    }

    public static CityPresenter saveInstance(CityPresenter cityPresenter) {
        if (CityManager.getInstance().getPresenterHash().containsKey(CityPresenter.class.getSimpleName())) {
            cityPresenter = (CityPresenter) CityManager.getInstance().getPresenterHash().
                    get(CityPresenter.class.getSimpleName());
        } else {
            cityPresenter = new CityPresenter();
            CityManager.getInstance().getPresenterHash().put(CityPresenter.class.getSimpleName(),
                    cityPresenter);
        }
        return cityPresenter;
    }
}

