package com.example.user.mvp.Presenter;

import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.example.user.mvp.Model.City;
import com.example.user.mvp.R;

import java.util.Arrays;
import java.util.Collections;

/**
 * Created by user on 02.11.17.
 */

public class CityAdapter extends RecyclerView.Adapter<CityAdapter.ViewHolder> {
    private City[] Dataset = new City[0];
    ItemTouchHelper itemTouchHelper;


    public CityAdapter(RecyclerView RecyclerView) {
        itemTouchHelper = new ItemTouchHelper(simpleCallback);
        itemTouchHelper.attachToRecyclerView(RecyclerView);

    }

    public City[] getDataset() {
        return Dataset;
    }

    public void setDataset(City[] Dataset) {
        this.Dataset = Dataset;
        this.notifyDataSetChanged();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final City city = Dataset[position];
        holder.TextViewName.setText(city.getName());
        holder.TextViewPopulation.setText(String.valueOf(city.getPopulation()));
        holder.TextViewName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(v.getContext(), "В городе " + city.getName() + " проживает " +
                        city.getPopulation() + " человек.", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return Dataset.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public final TextView TextViewName;
        public final TextView TextViewPopulation;

        public ViewHolder(View v) {
            super(v);
            TextViewName = v.findViewById(R.id.name_textview);
            TextViewPopulation = v.findViewById(R.id.population_textview);
        }
    }

    @Override
    public CityAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item, parent, false));
    }

    ItemTouchHelper.SimpleCallback simpleCallback = new ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP | ItemTouchHelper.DOWN, ItemTouchHelper.ACTION_STATE_DRAG) {
        @Override
        public boolean onMove(RecyclerView RecyclerView, RecyclerView.ViewHolder holder,
                              RecyclerView.ViewHolder target) {
            final int fromPosition = holder.getAdapterPosition();
            final int toPosition = target.getAdapterPosition();
            if (fromPosition < toPosition) {
                for (int i = fromPosition; i < toPosition; i++) {
                    Collections.swap(Arrays.asList(getDataset()), i, i + 1);
                }
            } else {
                for (int i = fromPosition; i > toPosition; i--) {
                    Collections.swap(Arrays.asList(getDataset()), i, i - 1);
                }
            }
            notifyItemMoved(fromPosition, toPosition);
            return true;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        }
    };
}

